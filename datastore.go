package main

import (
	"fmt"
	"log"
	"time"

	mgo "gopkg.in/mgo.v2"
)

func retry(attempts int, sleep time.Duration, callback func() (*mgo.Session, error)) (session *mgo.Session, err error) {
	for i := 0; ; i++ {
		session, err = callback()
		if err == nil {
			return
		}

		if i >= (attempts - 1) {
			break
		}

		time.Sleep(sleep)

		log.Println("retrying after error:", err)
	}
	return nil, fmt.Errorf("after %d attempts, last error: %s", attempts, err)
}

func CreateSession(host string) (*mgo.Session, error) {
	return retry(5, 2*time.Second, func() (*mgo.Session, error) {
		session, err := mgo.Dial(host)
		if err != nil {
			return nil, err
		}

		session.SetMode(mgo.Monotonic, true)

		return session, nil
	})
}
